package com.kolyadruz.forsequenia.app;

import com.kolyadruz.forsequenia.mvp.models.ResponseData;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SequeniaApi {

    @GET("films.json")
    public Call<ResponseData> getfilmslist();

}
