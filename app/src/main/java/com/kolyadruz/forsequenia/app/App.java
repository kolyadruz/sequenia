package com.kolyadruz.forsequenia.app;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.kolyadruz.forsequenia.di.AppComponent;
import com.kolyadruz.forsequenia.di.DaggerAppComponent;
import com.kolyadruz.forsequenia.di.modules.ContextModule;

public class App  extends Application {
    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();

    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @VisibleForTesting
    public static void setAppComponent(@NonNull AppComponent appComponent) {
        sAppComponent = appComponent;
    }
}