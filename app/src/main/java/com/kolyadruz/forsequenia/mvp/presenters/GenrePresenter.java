package com.kolyadruz.forsequenia.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.kolyadruz.forsequenia.mvp.views.GenreView;

@InjectViewState
public class GenrePresenter extends MvpPresenter<GenreView> {

    private String mGenre;

    public GenrePresenter(String genre) {
        super();

        mGenre = genre;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        getViewState().showGenre(mGenre);
    }
}
