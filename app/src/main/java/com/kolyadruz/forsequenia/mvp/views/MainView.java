package com.kolyadruz.forsequenia.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.models.ResponseData;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainView extends MvpView {

    void showError(String message);

    void showFilmsListWithGenres(ResponseData responseData);

    void showFilmDetail(Film film);

}
