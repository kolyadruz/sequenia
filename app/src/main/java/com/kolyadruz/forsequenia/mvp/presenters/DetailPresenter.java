package com.kolyadruz.forsequenia.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.views.DetailView;

@InjectViewState
public class DetailPresenter extends BasePresenter<DetailView> {

    private Film film;

    public DetailPresenter(Film film) {
        super();

        this.film = film;

    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().showDetail(film);
    }
}
