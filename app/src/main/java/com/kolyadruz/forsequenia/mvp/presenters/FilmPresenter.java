package com.kolyadruz.forsequenia.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.views.FilmView;

@InjectViewState
public class FilmPresenter extends MvpPresenter<FilmView> {

    private Film mFilm;

    public FilmPresenter(Film film) {
        super();

        mFilm = film;
    }


    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        getViewState().showFilm(mFilm);
    }
}
