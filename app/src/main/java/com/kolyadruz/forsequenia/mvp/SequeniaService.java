package com.kolyadruz.forsequenia.mvp;

import com.kolyadruz.forsequenia.mvp.models.ResponseData;
import com.kolyadruz.forsequenia.app.SequeniaApi;

import retrofit2.Call;

public class SequeniaService {

    private SequeniaApi mSequeniaApi;

    public SequeniaService(SequeniaApi sequeniaApi) {
        mSequeniaApi = sequeniaApi;
    }

    public Call<ResponseData> getfilmslist() {
        return mSequeniaApi.getfilmslist();
    }

}
