package com.kolyadruz.forsequenia.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.views.FilmsListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@InjectViewState
public class FilmsListPresenter extends BasePresenter<FilmsListView> {

    private static final String TAG = "FilmsListPresenter";

    private List<Film> mFilms;

    public FilmsListPresenter(List<Film> filmsList) {
        super();
        mFilms = filmsList;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        sortGenres(mFilms);

    }

    private void sortGenres(List<Film> filmsList) {

        List<String> genres = new ArrayList<>();

        for (Film film : filmsList) {

            for (String genre : film.getGenres()) {

                if (!genres.contains(genre)) {
                    genres.add(genre);
                }

            }

        }

        Collections.sort(genres);

        Log.d(TAG, "sortGenres: " + genres);

        getViewState().showGenres(genres);
        getViewState().showFilms(mFilms);

    }

    public void onGenreSelection(int position, String genre) {

        getViewState().setSelection(position);
        filterByGenre(genre);

    }

    public void filterByGenre(String genre) {

        List<Film> films = new ArrayList<>();

        for (Film film : mFilms) {

            if (film.getGenres().contains(genre)) {

                films.add(film);

            }

        }

        Collections.sort(films, (o1, o2) -> o1.getLocalized_name().compareToIgnoreCase(o2.getLocalized_name()));

        getViewState().showFilms(films);

    }

}
