package com.kolyadruz.forsequenia.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.kolyadruz.forsequenia.mvp.models.Film;

public interface DetailView extends MvpView {

    void showDetail(Film film);

}
