package com.kolyadruz.forsequenia.mvp.presenters;

import android.util.Log;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.forsequenia.app.App;
import com.kolyadruz.forsequenia.mvp.SequeniaService;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.models.ResponseData;
import com.kolyadruz.forsequenia.mvp.views.MainView;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {

    public static final String TAG = "MainPresenter";

    @Inject
    SequeniaService mSequeniaService;

    ResponseData responseData;

    private boolean isLoading;

    public MainPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadFilms();
    }

    public void loadFilms() {
        if (this.isLoading) {
            return;
        }

        this.isLoading = true;

        Call<ResponseData> call = mSequeniaService.getfilmslist();

        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(@NonNull Call<ResponseData> call, @NonNull Response<ResponseData> response) {

                responseData = response.body();

                Gson gson = new Gson();

                String mainDataStr = gson.toJson(responseData);

                Log.d(TAG, "loadData: " + mainDataStr);

                showAllFilms();

            }

            @Override
            public void onFailure(@NonNull Call<ResponseData> call, @NonNull Throwable t) {

                t.printStackTrace();
            }
        });
    }

    public void showAllFilms() {

        getViewState().showFilmsListWithGenres(responseData);

    }

    public void showFilmDetail(Film film) {
        getViewState().showFilmDetail(film);
    }

}
