package com.kolyadruz.forsequenia.mvp.views;

import com.arellomobile.mvp.MvpView;

public interface GenreView extends MvpView {

    void showGenre(String genre);

}
