package com.kolyadruz.forsequenia.mvp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ResponseData implements Serializable {

    @SerializedName("films")
    ArrayList<Film> films;

    public ArrayList<Film> getFilms() {
        return films;
    }

    public void setFilms(ArrayList<Film> films) {
        this.films = films;
    }
}
