package com.kolyadruz.forsequenia.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.kolyadruz.forsequenia.mvp.models.Film;

public interface FilmView extends MvpView {
    void showFilm(Film film);
}
