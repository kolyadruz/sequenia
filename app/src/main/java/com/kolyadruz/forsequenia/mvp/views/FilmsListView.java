package com.kolyadruz.forsequenia.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.kolyadruz.forsequenia.mvp.models.Film;

import java.util.List;

public interface FilmsListView extends MvpView {

    void showGenres(List<String> genres);
    void showFilms(List<Film> filmList);
    void setSelection(int position);

}
