package com.kolyadruz.forsequenia.di.modules;

import com.kolyadruz.forsequenia.mvp.SequeniaService;
import com.kolyadruz.forsequenia.app.SequeniaApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class})
public class SequeniaModule {
    @Provides
    @Singleton
    public SequeniaService provideSequeniaService(SequeniaApi authApi) {
        return new SequeniaService(authApi);
    }


}
