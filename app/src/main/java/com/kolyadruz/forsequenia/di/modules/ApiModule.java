package com.kolyadruz.forsequenia.di.modules;

import com.kolyadruz.forsequenia.app.SequeniaApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {RetrofitModule.class})
public class ApiModule {

    @Provides
    @Singleton
    public SequeniaApi provideAuthApi(Retrofit retrofit) {
        return retrofit.create(SequeniaApi.class);
    }

}
