package com.kolyadruz.forsequenia.di;

import android.content.Context;

import com.kolyadruz.forsequenia.di.modules.ContextModule;
import com.kolyadruz.forsequenia.di.modules.SequeniaModule;
import com.kolyadruz.forsequenia.mvp.SequeniaService;
import com.kolyadruz.forsequenia.mvp.presenters.MainPresenter;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ContextModule.class, SequeniaModule.class})

public interface AppComponent {

    Context getContext();
    SequeniaService getNetworkService();

    void inject(MainPresenter presenter);

}
