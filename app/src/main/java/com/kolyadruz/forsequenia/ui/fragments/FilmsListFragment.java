package com.kolyadruz.forsequenia.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.forsequenia.R;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.models.ResponseData;
import com.kolyadruz.forsequenia.mvp.presenters.FilmsListPresenter;
import com.kolyadruz.forsequenia.mvp.views.FilmsListView;
import com.kolyadruz.forsequenia.ui.activities.MainActivity;
import com.kolyadruz.forsequenia.ui.adapters.FilmsListAdapter;
import com.kolyadruz.forsequenia.ui.adapters.GenresListAdapter;
import com.kolyadruz.forsequenia.ui.adapters.OnFilmClickedListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilmsListFragment extends MvpAppCompatFragment implements FilmsListView {

    public static final String ARGS_FILMS = "argsFilms";

    @InjectPresenter
    FilmsListPresenter mFilmsListPresenter;

    private ResponseData mResponseData;
    private OnFilmClickedListener onFilmClickedListener;

    @BindView(R.id.genres_listview)
    ListView mGenresListView;

    @BindView(R.id.films_grid)
    RecyclerView mFilmsListGrid;

    @ProvidePresenter
    FilmsListPresenter provideFilmsListPresenter() {
        mResponseData = (ResponseData) getArguments().get(ARGS_FILMS);

        return new FilmsListPresenter(mResponseData.getFilms());
    }


    private GenresListAdapter mGenresListAdapter;
    private FilmsListAdapter mFilmsListAdapter;


    public static FilmsListFragment getInstance(ResponseData responseData) {

        FilmsListFragment filmsListFragment = new FilmsListFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARGS_FILMS, responseData);

        filmsListFragment.setArguments(args);

        return filmsListFragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mGenresListAdapter = new GenresListAdapter(getMvpDelegate());
        mGenresListView.setAdapter(mGenresListAdapter);
        mGenresListView.setOnItemClickListener((parent, v, position, id) -> {
           mFilmsListPresenter.onGenreSelection(position, mGenresListAdapter.getItem(position));
        });

        mFilmsListAdapter = new FilmsListAdapter(getContext() , onFilmClickedListener);
        mFilmsListGrid.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.VERTICAL, false);
        mFilmsListGrid.setLayoutManager(layoutManager);
        mFilmsListGrid.setAdapter(mFilmsListAdapter);

    }

    @Override
    public void showGenres(List<String> genres) {
        mGenresListAdapter.setGenres(genres);
    }

    @Override
    public void showFilms(List<Film> filmsList) {
        mFilmsListAdapter.updateFilmList(filmsList);
    }

    @Override
    public void setSelection(int position) {
        mGenresListAdapter.setSelection(position);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onFilmClickedListener = (MainActivity)context;
        } catch (ClassCastException cce) {

        }
    }
}
