package com.kolyadruz.forsequenia.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpDelegate;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.forsequenia.R;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.presenters.GenrePresenter;
import com.kolyadruz.forsequenia.mvp.views.GenreView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenresListAdapter  extends MvpBaseAdapter {

    private List<String> mGenres;

    private int mSelection = -1;

    public GenresListAdapter(MvpDelegate<?> parentDelegate) {
        super(parentDelegate, String.valueOf(0));

        mGenres = new ArrayList<>();

    }

    public void setGenres(List<String> genres) {
        mGenres = genres;
        notifyDataSetChanged();
    }

    public void setSelection(int selection) {
        mSelection = selection;

        notifyDataSetChanged();
    }

    public int getFilmsCount() {
        return mGenres.size();
    }

    @Override
    public int getCount() {
        return mGenres.size();
    }

    @Override
    public String getItem(int position) {
        return mGenres.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        GenreHolder holder;
        if (convertView != null) {
            holder = (GenreHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_genre, parent, false);
            holder = new GenreHolder(convertView);
            convertView.setTag(holder);
        }

        final String genre = getItem(position);

        holder.bind(position, genre);

        return convertView;
    }

    public class GenreHolder implements GenreView {

        @InjectPresenter
        GenrePresenter mGenrePresenter;

        private String mGenre;

        @BindView(R.id.tv_genre_name)
        TextView nameTextView;
        View view;

        private MvpDelegate mMvpDelegate;

        @ProvidePresenter
        GenrePresenter provideFilmPresenter() {
            return new GenrePresenter(mGenre);
        }

        GenreHolder(View view) {
            this.view = view;

            ButterKnife.bind(this, view);
        }

        void bind(int position, String genre) {
            if (getMvpDelegate() != null) {
                getMvpDelegate().onSaveInstanceState();
                getMvpDelegate().onDetach();
                getMvpDelegate().onDestroyView();
                mMvpDelegate = null;
            }

            mGenre = genre;

            getMvpDelegate().onCreate();
            getMvpDelegate().onAttach();

            view.setBackgroundResource(position == mSelection ? R.color.colorAccent : android.R.color.transparent);
        }

        @Override
        public void showGenre(String genre) {
            nameTextView.setText(genre);
        }

        MvpDelegate getMvpDelegate() {
            if (mGenre == null) {
                return null;
            }

            if (mMvpDelegate == null) {
                mMvpDelegate = new MvpDelegate<>(this);
                mMvpDelegate.setParentDelegate(GenresListAdapter.this.getMvpDelegate(), String.valueOf(mGenre));

            }
            return mMvpDelegate;
        }
    }

}
