package com.kolyadruz.forsequenia.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;
import com.kolyadruz.forsequenia.R;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.presenters.DetailPresenter;
import com.kolyadruz.forsequenia.mvp.views.DetailView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailFragment extends MvpAppCompatFragment implements DetailView {

    public static final String ARGS_FILM = "argsFilm";

    @InjectPresenter
    DetailPresenter mDetailPresenter;

    private Film film;

    @BindView(R.id.poster)
    ImageView poster;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.year)
    TextView year;

    @BindView(R.id.rating)
    TextView rating;

    @BindView(R.id.description)
    TextView descritpion;

    @ProvidePresenter
    DetailPresenter provideDetailPresenter() {

        film = (Film) getArguments().get(ARGS_FILM);

        return new DetailPresenter(film);

    }

    public static DetailFragment getInstance(Film film) {

        DetailFragment detailFragment = new DetailFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARGS_FILM, film);

        detailFragment.setArguments(args);

        return detailFragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_film_description, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

    }


    @Override
    public void showDetail(Film film) {

        Glide.with(getContext()).load(film.getImage_url()).into(poster);
        name.setText(film.getName());
        year.setText(film.getYear().toString());
        rating.setText(film.getRating().toString());
        descritpion.setText(film.getDescription());

    }
}
