package com.kolyadruz.forsequenia.ui.adapters;

import com.kolyadruz.forsequenia.mvp.models.Film;

public interface OnFilmClickedListener {

    void onItemClick(Film film);

}
