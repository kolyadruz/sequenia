package com.kolyadruz.forsequenia.ui.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.kolyadruz.forsequenia.R;
import com.kolyadruz.forsequenia.mvp.models.Film;
import com.kolyadruz.forsequenia.mvp.models.ResponseData;
import com.kolyadruz.forsequenia.mvp.presenters.MainPresenter;
import com.kolyadruz.forsequenia.mvp.views.MainView;
import com.kolyadruz.forsequenia.ui.adapters.OnFilmClickedListener;
import com.kolyadruz.forsequenia.ui.fragments.DetailFragment;
import com.kolyadruz.forsequenia.ui.fragments.FilmsListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MvpAppCompatActivity implements MainView, OnFilmClickedListener {

    @InjectPresenter
    MainPresenter mMainPresenter;

    @BindView(R.id.activity_home_toolbar)
    Toolbar mToolBar;

    @BindView(R.id.progress)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setSupportActionBar(mToolBar);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getApplicationContext(), "Ошибка при запросе данных", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showFilmsListWithGenres(ResponseData responseData) {
        mProgressBar.setVisibility(View.GONE);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, FilmsListFragment.getInstance(responseData))
                .commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Фильмы");
    }

    @Override
    public void showFilmDetail(Film film) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, DetailFragment.getInstance(film))
                .commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(film.getLocalized_name());
    }

    @Override
    public void onItemClick(Film film) {
        mMainPresenter.showFilmDetail(film);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();

                mMainPresenter.showAllFilms();

                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
