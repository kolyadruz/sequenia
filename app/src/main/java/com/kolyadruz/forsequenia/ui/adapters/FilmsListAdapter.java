package com.kolyadruz.forsequenia.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kolyadruz.forsequenia.R;
import com.kolyadruz.forsequenia.mvp.models.Film;

import java.util.ArrayList;
import java.util.List;

public class FilmsListAdapter extends RecyclerView.Adapter<FilmsListAdapter.FilmViewHolder>  {

    Context context;
    List<Film> films;

    private OnFilmClickedListener onFilmClickedListener;

    public FilmsListAdapter(Context context, OnFilmClickedListener onFilmClickedListener) {

        this.context = context;;
        this.films = new ArrayList<>();


        this.onFilmClickedListener = onFilmClickedListener;
    }

    @NonNull
    @Override
    public FilmsListAdapter.FilmViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_film, viewGroup, false);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemView.setLayoutParams(lp);

        return new FilmViewHolder(itemView, onFilmClickedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull FilmsListAdapter.FilmViewHolder filmViewHolder, int position) {

        final Film film = films.get(position);

        Glide.with(context).load(film.getImage_url()).into(filmViewHolder.poster);

        filmViewHolder.title.setText(film.getLocalized_name());

    }

    @Override
    public int getItemCount() {
        return films.size();
    }

    public void updateFilmList(List<Film> filmsList) {
        films = filmsList;
        notifyDataSetChanged();
    }

    public class FilmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView poster;
        TextView title;

        OnFilmClickedListener onFilmClickedListener;

        public FilmViewHolder(@NonNull View itemView, OnFilmClickedListener onFilmClickedListener) {
            super(itemView);
            poster = itemView.findViewById(R.id.poster);
            title = itemView.findViewById(R.id.title);

            this.onFilmClickedListener = onFilmClickedListener;
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onFilmClickedListener.onItemClick(films.get(getAdapterPosition()));
        }
    }

}
